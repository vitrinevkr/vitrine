chrome.app.runtime.onLaunched.addListener(function(launchData) {
  chrome.app.window.create(
    'application.html',
    {
      id: 'vitrinevkr',
    },
    function(win) {
      win.contentWindow.launchData = launchData;
      win.maximize();
      win.show();
      win.fullscreen();
    }
  );
});
