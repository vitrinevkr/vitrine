Необходимо установить программную платформу Node.js, для того чтобы дать JavaScript возможность взаимодействовать с устройствами ввода-вывода через свой API и подключать разные внешние библиотеки, и пакетный менеджер для JavaScript yarn или npm.
Node.js v >= 6

Клонировать проект из репозитория:
$ git clone https://vitrinevkr@bitbucket.org/vitrinevkr/vitrine.git

Перейти в директорию проекта и запустить:
$ cd vitrine
$ yarn install

После установки всех необходимых пакетов запустить:
develop:
$ yarn start

production:
$ yarn build

Деплой на firebase:
yarn global add firebase-tools

Перейти в каталог build.

Войти в аккаунт Google:
firebase login

Инициирование проекта:
firebase init

Развертывание проекта:
firebase deploy

URL развернутого проекта:
https://vitrinevkr.firebaseapp.com/
