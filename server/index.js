const path = require('path');
const express = require('express');
const proxy = require('json-proxy');

const app = express();

if (process.env.NODE_ENV === 'development') {
  const webpack = require('webpack');
  const config = require('../config/webpack.development');
  const compiler = webpack(config);
  const middleware = require('webpack-dev-middleware')(compiler, {
    publicPath: config.output.publicPath,
    stats: {
      colors: true,
      chunks: false,
    },
  });

  app.use(middleware);
  app.use(require('webpack-hot-middleware')(compiler));
  app.use(express.static(path.resolve(__dirname, '../assets')));

  app.get('/assets/:name', (req, res) => {
    res.sendFile(path.resolve(__dirname, `../assets/${req.params.name}`));
  });
  app.get('/assets/:dir/:name', (req, res) => {
    res.sendFile(path.resolve(__dirname, `../assets/${req.params.dir}/${req.params.name}`));
  });
  app.get('/assets/:dir1/:dir2/:name', (req, res) => {
    res.sendFile(
      path.resolve(__dirname, `../assets/${req.params.dir1}/${req.params.dir2}/${req.params.name}`)
    );
  });

  app.get('*', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(
      middleware.fileSystem.readFileSync(
        path.join(config.output.path, 'index.html')
      )
    );
  });
} else {
  app.use(express.static(path.resolve(__dirname, '../build')));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../build/index.html'));
  });
}

app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'), () => {
  console.log('Express server listening on port ' + app.get('port')); // eslint-disable-line
});
