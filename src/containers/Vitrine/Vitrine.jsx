import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Swiper from 'react-id-swiper';
import classNames from 'classnames/bind';

import Card from 'src/components/VitrineCard/VitrineCard';

import styles from './Vitrine.pcss';

const cx = classNames.bind(styles);

class Vitrine extends React.PureComponent {
  componentDidMount() {}

  onSlideChangeStart = ({ activeIndex }) => {
    const { setActiveIndex } = this.props;

    if (activeIndex !== this.props.activeIndex) {
      setActiveIndex(activeIndex);
    }
  };

  onTap = swiper => {
    const { activeIndex } = this.props;
    if (swiper.clickedIndex > activeIndex) {
      swiper.slideNext();
    } else if (swiper.clickedIndex < activeIndex) {
      swiper.slidePrev();
    }
  };

  renderSliders() {
    const { activeIndex, catalog, items, setFlipCard } = this.props;
    const elements = [];
    items.forEach((v, index) => {
      elements.push(
        <div className={cx('item')} key={`${v.id}-${index}`}>
          <Card
            data={v}
            index={index}
            active={activeIndex === index}
            parentCatalog={catalog[v.parentCatalogId]}
            setFlipCard={setFlipCard}
          />
        </div>
      );
    });

    return elements;
  }

  render() {
    const { activeIndex, swiperSettings } = this.props;

    return (
      <div className={cx('layout')}>
        <Swiper
          {...swiperSettings}
          initialSlide={activeIndex}
          onSlideChangeStart={this.onSlideChangeStart}
          onTap={this.onTap}
        >
          {this.renderSliders()}
        </Swiper>
      </div>
    );
  }
}

Vitrine.propTypes = {
  activeIndex: PropTypes.number.isRequired,
  location: PropTypes.object.isRequired,
  setActiveIndex: PropTypes.func.isRequired,
  setFlipCard: PropTypes.func.isRequired,
  swiperSettings: PropTypes.object.isRequired,
};

export default connect(
  state => ({
    ...state.vitrine,
  }),
  dispatch => ({
    setActiveIndex: payload => dispatch({ type: 'VITRINE_SET_ACTIVE_INDEX', payload }),
    setFlipCard: payload => dispatch({ type: 'VITRINE_SET_FLIP_CARD', payload }),
  })
)(Vitrine);
