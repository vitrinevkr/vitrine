import { SWIPER_VITRINE } from 'src/constants/settings';
import { CATALOG, CATALOG_IDS, VITRINE } from 'src/stubData/vitrine';

const initialState = {
  activeIndex: SWIPER_VITRINE.initialSlide,
  catalog: CATALOG,
  catalogIds: CATALOG_IDS,
  items: VITRINE,
  flipCards: [],
  swiperSettings: {
    ...SWIPER_VITRINE,
  },
};

const vitrineReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'VITRINE_SET_ACTIVE_INDEX': {
      return {
        ...state,
        activeIndex: payload,
      };
    }
    case 'VITRINE_SET_FLIP_CARD': {
      const index = state.flipCards.findIndex(index => index === payload);
      if (~index) {
        return {
          ...state,
          flipCards: [
            ...state.flipCards.slice(0, index),
            ...state.flipCards.slice(index + 1),
          ],
        };
      }
      return {
        ...state,
        flipCards: [...state.flipCards, payload],
      };
    }
  }

  return state;
};

export default vitrineReducer;
