import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Swiper from 'react-id-swiper';
import { makeGetSwiperData } from './selector';

import Card from 'src/components/Card/Card';

import classNames from 'classnames/bind';
import styles from './Catalog.pcss';
const cx = classNames.bind(styles);

class Catalog extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      activeIndex: props.activeIndex,
    };
  }

  onSlideChangeStart = ({ activeIndex }) => {
    if (activeIndex !== this.state.activeIndex) {
      this.setState({
        activeIndex,
      });
    }
  };

  onTap = swiper => {
    if (swiper.clickedIndex > this.state.activeIndex) {
      swiper.slideNext();
    } else if (swiper.clickedIndex < this.state.activeIndex) {
      swiper.slidePrev();
    }
  };

  renderSliders() {
    const { products, history, catalogId } = this.props;
    const { activeIndex } = this.state;
    const elements = [];

    products.forEach((product, index) => {
      elements.push(
        <div className={cx('item')} key={index}>
          <Card
            data={product}
            active={activeIndex === index}
            swiper={Math.abs(activeIndex - index) < 3}
            catalogId={catalogId}
            onClose={history.goBack}
          />
        </div>
      );
    });

    return elements;
  }

  render() {
    const { activeIndex, products, swiperSettings } = this.props;

    return (
      <div className={cx('layout')}>
        {products.length ? (
          <Swiper
            {...swiperSettings}
            initialSlide={activeIndex}
            onSlideChangeStart={this.onSlideChangeStart}
            onTap={this.onTap}
          >
            {this.renderSliders()}
          </Swiper>
        ) : null}
      </div>
    );
  }
}

Catalog.propTypes = {
  products: PropTypes.array.isRequired,
  activeIndex: PropTypes.number.isRequired,
  catalogId: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

Catalog.defaultProps = {};

const makeMapStateToProps = () => {
  const getProductsSwiper = makeGetSwiperData();
  const mapStateToProps = (state, props) => getProductsSwiper(state, props);
  return mapStateToProps;
};

export default connect(makeMapStateToProps)(Catalog);
