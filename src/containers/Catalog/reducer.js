import { SWIPER_VITRINE } from 'src/constants/settings';
import { PRODUCT_IDS } from 'src/stubData/catalogs';
import { PRODUCTS } from 'src/stubData/products';
import { SPECS } from 'src/stubData/specs';

const initialState = {
  items: PRODUCTS,
  productIds: PRODUCT_IDS,
  intags: SPECS,
  swiperSettings: { ...SWIPER_VITRINE },
};

const productsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'SET_PRODUCT_INFO': {
      return {
        ...state,
        intags: {
          ...state.intags,
          [payload.id]: payload.intags,
        },
      };
    }
  }

  return state;
};

export default productsReducer;
