import { createSelector } from 'reselect';

const getProductIds = ({ products }, { match }) => {
  const { catalogId } = match.params;
  return products.productIds[catalogId] || [];
};

const getItems = ({ products }) => products.items;

export const makeGetProducts = () =>
  createSelector([getProductIds, getItems], (productIds, items) => {
    const products = [];
    productIds.forEach(id => {
      if (items[id]) {
        products.push(items[id]);
      }
    });

    return products;
  });

const getSwiperData = (state, { match }) => ({
  id: +match.params.id,
  catalogId: match.params.catalogId,
  swiperSettings: state.products.swiperSettings,
});

export const makeGetSwiperData = () =>
  createSelector([makeGetProducts(), getSwiperData], (products, swiper) => {
    const index = products.findIndex(p => p.id === swiper.id);

    return {
      products,
      activeIndex: ~index ? index : 0,
      ...swiper,
    };
  });
