import { MENU } from 'src/stubData/vitrine';

const initialState = {
  active: false,
  items: MENU,
};

const vitrineReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'MENU_CLOSE': {
      return {
        ...state,
        active: false,
      };
    }
    case 'MENU_TOGGLE': {
      return {
        ...state,
        active: !state.active,
      };
    }
  }

  return state;
};

export default vitrineReducer;
