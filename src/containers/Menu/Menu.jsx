import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import classNames from 'classnames/bind';
import styles from './Menu.pcss';
const cx = classNames.bind(styles);

class Menu extends React.PureComponent {
  render() {
    const { active, activeSpeech, close, items, toggle } = this.props;

    return (
      <div className={cx('wrapper', { blmb: !active && !activeSpeech })}>
        <nav className={cx('menu', { active })}>
          <button className={cx('menu-open-button')} onClick={toggle}>
            <span className={cx('hamburger', 'hamburger-1')} />
            <span className={cx('hamburger', 'hamburger-2')} />
            <span className={cx('hamburger', 'hamburger-3')} />
          </button>
          <NavLink to="/" className={cx('menu-item')} onClick={close}>
            <i className={cx('fa fa-home')} />
          </NavLink>
          {items.map(i => (
            <NavLink to={`/catalog/${i.id}`} key={i.id} className={cx('menu-item')} onClick={close}>
              <i className={`fa fa-${i.icon}`} />
            </NavLink>
          ))}
        </nav>
      </div>
    );
  }
}

Menu.propTypes = {
  active: PropTypes.bool.isRequired,
  items: PropTypes.array.isRequired,
  toggle: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    active: state.menu.active,
    items: state.menu.items,
    activeSpeech: state.speech.active,
  }),
  dispatch => ({
    close: () => dispatch({ type: 'MENU_CLOSE' }),
    toggle: () => dispatch({ type: 'MENU_TOGGLE' }),
  })
)(Menu);
