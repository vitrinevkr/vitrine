import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';

import { CATALOG, VITRINE } from 'src/stubData/vitrine';
import { SPECS } from 'src/stubData/specs';
import { PRODUCT_IDS } from 'src/stubData/catalogs';
import { PRODUCTS } from 'src/stubData/products';

import styles from './Test.pcss';

const cx = classNames.bind(styles);

class Test extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentWillMount() {}

  render() {
    const { items } = this.props;

    return (
      <div className={cx('layout')}>
        {/*{Object.values(items).map(({ images }) => [*/}
        {/*...images.map(i => <img src={i} key={i} alt="" />),*/}
        {/*])}*/}
      </div>
    );
  }
}

Test.propTypes = {
  location: PropTypes.object.isRequired,
};

export default connect(
  state => {
    // const tmp = Object.entries(state.productsState.items)
    //   .filter(([id, mp]) => SPECS[id])
    //   .reduce((p, [id, mp]) => ({...p, [id]: mp}), {});
    // console.log('state', tmp, state);

    // const tmp = Object.values(PRODUCT_IDS).reduce((p, c) => [...p, ...c], []);
    // const items = Object.entries(SPECS)
    //   .filter(([id, v]) => tmp.includes(+id))
    //   .reduce((p, [id, c]) => ({ ...p, [id]: c }), {});

    const tmp = Object.values(PRODUCTS).reduce(
      (p, c) => ({
        ...p,
        [c.id]: {
          ...c,
          oldPrice: c.newPrice > 6000 ? c.newPrice + 2000 : c.oldPrice,
          newPrice: c.newPrice > 9000 ? c.newPrice - 1000 : c.newPrice,
        },
      }),
      {}
    );

    console.log('-!!!', tmp);

    return {
      ...state.products,
    };
  },
  dispatch => ({
    init: () => {},
  })
)(Test);
