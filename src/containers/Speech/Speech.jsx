import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { checkSpeech } from 'src/utils/speech';

import classNames from 'classnames/bind';
import styles from './Speech.pcss';

const cx = classNames.bind(styles);

class Speech extends React.PureComponent {
  constructor(props) {
    super(props);
    this.speechRecognition = new webkitSpeechRecognition();
    this.speechRecognition.lang = 'ru';
    this.speechRecognition.continuous = true;
    this.speechRecognition.interimResults = true;
  }

  componentDidMount() {
    const { redirect, setText } = this.props;

    this.speechRecognition.onstart = e => {};

    this.speechRecognition.onresult = e => {
      // console.log('speechRecognition.onresult', e);
      let searchText = '';
      for (let i = e.resultIndex; i < e.results.length; ++i) {
        if (e.results[i].isFinal) {
          searchText += e.results[i][0].transcript;
        }
      }
      const st = checkSpeech(searchText);
      if (st) {
        this.speechRecognition.stop();
        if (typeof +st === 'number') {
          redirect(`/catalog/${st}`);
        }
        if (st === 'home') {
          redirect('/');
        }
      } else {
        setText(searchText);
      }
    };

    this.speechRecognition.onerror = e => {
      const { setNetworkError } = this.props;
      if (e.error === 'network') {
        setNetworkError();
      }
      console.error(e.error);
    };

    this.speechRecognition.onend = e => {
      const { reset } = this.props;
      reset();
    };
  }

  componentWillReceiveProps(nextProps) {
    const { active, reset } = this.props;
    if (active && nextProps.activeMenu) {
      this.speechRecognition.stop();
      reset();
    }
  }

  componentWillUnmount() {
    this.speechRecognition.abort();
  }

  handleClick = e => {
    e.stopPropagation();
    const { active, activeMenu, reset, setActive } = this.props;
    if (active) {
      this.speechRecognition.stop();
      reset();
    } else if (!activeMenu) {
      setActive();
      this.speechRecognition.start();
    }
  };

  render() {
    const { active, activeMenu, networkError, searchText } = this.props;

    return (
      <div className={cx('wrapper')}>
        <button
          className={cx('btn', { btnActive: active })}
          disabled={activeMenu || networkError}
          onClick={this.handleClick}
        >
          <i className="fa fa-3x fa-microphone" aria-hidden="true" />
        </button>
        {searchText && (
          <div className={cx('')}>
            <div className={cx('startText')}>{!!searchText ? `[${searchText}?]` : '...'}</div>
          </div>
        )}
        {active && (
          <div className={cx('tip')}>Назовите экран, например, «экран телефоны» или «телефоны»</div>
        )}
        {networkError && <div className={cx('tip')}>Нет доступа к сети интернет</div>}
      </div>
    );
  }
}

Speech.propTypes = {
  active: PropTypes.bool.isRequired,
  networkError: PropTypes.bool.isRequired,
  searchText: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  hidden: PropTypes.bool,
};

Speech.defaultProps = {
  disabled: false,
  hidden: false,
  networkError: false,
};

export default connect(
  state => ({
    ...state.speech,
    activeMenu: state.menu.active,
  }),
  dispatch => ({
    redirect: url => {
      dispatch(push(url));
      dispatch({ type: 'SPEECH_RESET' });
    },
    reset: () => dispatch({ type: 'SPEECH_RESET' }),
    setActive: () => dispatch({ type: 'SPEECH_SET_ACTIVE' }),
    setNetworkError: () => dispatch({ type: 'SET_NETWORK_ERROR' }),
    setText: payload => dispatch({ type: 'SPEECH_SET_TEXT', payload }),
  })
)(Speech);
