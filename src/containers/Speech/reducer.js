const initialState = {
  active: false,
  networkError: false,
  searchStarted: false,
  searchText: '',
};

const speechReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'SPEECH_SET_ACTIVE': {
      return {
        ...state,
        active: true,
      };
    }
    case 'SPEECH_SET_TEXT': {
      return {
        ...state,
        searchText: payload,
      };
    }
    case 'SPEECH_RESET': {
      return {
        ...initialState,
        networkError: state.networkError,
      };
    }
    case 'SET_NETWORK_ERROR': {
      return {
        ...state,
        networkError: true,
      };
    }
  }

  return state;
};

export default speechReducer;
