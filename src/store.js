import { compose, createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import {
  syncHistoryWithStore,
  routerReducer,
  routerMiddleware,
} from 'react-router-redux';
import { history } from 'src/utils/history';

import reducers from 'src/reducers';

const enhancer = compose(
  applyMiddleware(routerMiddleware(history, thunkMiddleware)),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

const store = createStore(reducers, enhancer);

export default store;
