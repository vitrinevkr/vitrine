export const API_URL = '/api/public/v1';
export const TOKEN = '298abf32655cf248cf1f9e5a3e59abf9bcd098c5';
export const REGION_ID = 98140;
export const POINT_ID = '0006';
import store from 'src/store';

const params = `api_key=${TOKEN}&market_region=${REGION_ID}&point_codes=${POINT_ID}`;

// ,intags_categories,recommendations
// ,extended_remains_number,badges,sales

export function getProductInfo(id) {
  fetch(
    `${API_URL}/products/?api_key=${TOKEN}&market_region=${REGION_ID}&ids=${id}&params=id,intags_categories`
  )
    .then(response => response.json())
    .then(data => {
      if (data.length) {
        store.dispatch({
          type: 'SET_PRODUCT_INFO',
          payload: {
            id: data[0].id,
            intags: data[0].intags_categories,
          },
        });
      }
    })
    .catch(error => {
      console.log('getProductInfo error: ', id, error); // eslint-disable-line
    });
}
