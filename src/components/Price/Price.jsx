import React from "react";
import PropTypes from "prop-types";

import * as format from "src/utils/format-string";

import classNames from "classnames/bind";
import styles from "./Price.pcss";
const cx = classNames.bind(styles);

const Price = ({ newPrice, oldPrice, className }) => (
  <div className={cx("layout", className)}>
    {oldPrice
      ? <div className={cx("wrapper")}>
          <div className={cx("old")}>{format.price(oldPrice)}</div>
        </div>
      : null}
    <div className={cx("wrapper")}>
      <div className={cx("price")}>{format.price(newPrice)}</div>
    </div>
  </div>
);

Price.defaultProps = {
  newPrice: "",
  oldPrice: "",
  className: ""
};

Price.propTypes = {
  newPrice: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  oldPrice: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  className: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.string
  ])
};

export default Price;
