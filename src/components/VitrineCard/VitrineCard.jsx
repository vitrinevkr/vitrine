import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import FlipButton from 'src/components/FlipButton/FlipButton';

import classNames from 'classnames/bind';
import styles from './VitrineCard.pcss';
const cx = classNames.bind(styles);

class VitrineCard extends React.PureComponent {
  handleClickFlip = e => {
    e.stopPropagation();
    const { index, setFlipCard } = this.props;
    setFlipCard(index);
  };

  render() {
    const { active, catalogIds, data, flipCards, index, parentCatalog } = this.props;

    return (
      <div
        className={cx('layout', {
          active,
          flip: flipCards.includes(index),
        })}
      >
        <div className={cx('card', 'front')}>
          <Link
            className={cx('frontImage')}
            to={`/catalog/${data.catalogId}/${data.id}`}
            style={{
              backgroundImage: `url(${data.images[0]} )`,
            }}
          />
          <Link className={cx('frontTitle')} to={`/catalog/${data.catalogId}/${data.id}`}>
            {catalogIds[data.catalogId]}
          </Link>
          {parentCatalog.length > 1 &&
            <div className={cx('flipButton')}>
              <FlipButton onClick={this.handleClickFlip}>
                {catalogIds[data.parentCatalogId]}
              </FlipButton>
            </div>}
        </div>

        {parentCatalog.length > 1 &&
          <div className={cx('card', 'back')}>
            <div className={cx('backTitle')}>
              {catalogIds[data.parentCatalogId]}
            </div>
            <div className={cx('backContent')}>
              {parentCatalog.map(c => (
                <Link className={cx('backLink')} to={`/catalog/${c.id}`} key={c.id}>
                  {c.title}
                </Link>
              ))}
            </div>
            <div className={cx('flipButton')}>
              <FlipButton onClick={this.handleClickFlip}>
                {catalogIds[data.parentCatalogId]}
              </FlipButton>
            </div>
          </div>}
      </div>
    );
  }
}

VitrineCard.propTypes = {
  active: PropTypes.bool,
  parentCatalog: PropTypes.array.isRequired,
  catalogIds: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
  flipCards: PropTypes.array.isRequired,
};

VitrineCard.defaultProps = {
  active: false,
};

export default connect(
  state => ({
    catalogIds: state.vitrine.catalogIds,
    flipCards: state.vitrine.flipCards,
  }),
  dispatch => ({
    init: () => {},
    setActiveIndex: payload => dispatch({ type: 'VITRINE_SET_ACTIVE_INDEX', payload }),
    setFlipCard: payload => dispatch({ type: 'VITRINE_SET_FLIP_CARD', payload }),
  })
)(VitrineCard);
