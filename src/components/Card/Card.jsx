import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Swiper from 'react-id-swiper';
import { SWIPER_CARD } from 'src/constants/settings';

import * as api from 'src/api/products';
import * as format from 'src/utils/format-string';

import Price from 'src/components/Price/Price';
import FlipButton from 'src/components/FlipButton/FlipButton';
import Parameters from 'src/components/Parameters/Parameters';

import classNames from 'classnames/bind';
import styles from './Card.pcss';
const cx = classNames.bind(styles);

class Card extends Component {
  constructor(props) {
    super(props);

    this.state = {
      flip: false,
      activeIndex: 0,
      redirect: false,
    };
  }

  onSlideChangeStart = swiper => {
    if (swiper.activeIndex !== this.state.activeIndex) {
      this.setState({
        activeIndex: swiper.activeIndex,
      });
    }
  };

  onTap = swiper => {
    if (swiper.clickedIndex < this.state.activeIndex) {
      swiper.slidePrev();
    } else {
      swiper.slideNext();
    }
  };

  handleClickFlip = () => {
    this.setState({
      flip: !this.state.flip,
    });
  };

  render() {
    const { data, active, swiper, onClose } = this.props;

    const { id, article, images, name, oldPrice, newPrice } = data;
    const intags = this.props.intags[id];
    const remains = this.props.remains[id];

    return (
      <div
        className={cx('layout', {
          active,
          flip: this.state.flip,
        })}
      >
        {swiper ? (
          <div className={cx('card', 'front')}>
            {active && (
              <button className={cx('close')} onClick={onClose}>
                <i className={cx('fa', 'fa-2x', 'fa-close', 'icon')} />
              </button>
            )}
            <div className={cx('imageSwiper')}>
              {images && images.length > 1 ? (
                <Swiper
                  {...SWIPER_CARD}
                  onSlideChangeStart={this.onSlideChangeStart}
                  onTap={this.onTap}
                >
                  {images.map((image, key) => (
                    <div
                      className={cx('imageSlide')}
                      key={key}
                      style={{ backgroundImage: `url(${image} )` }}
                    />
                  ))}
                </Swiper>
              ) : (
                <div className={cx('image')} />
              )}
            </div>
            {/*{id}*/}
            <div className={cx('frontInfo')}>
              <div className={cx('frontTitle')}>{format.productName(name)}</div>
              <Price newPrice={newPrice} oldPrice={oldPrice} />
            </div>
            <div className={cx('flipButton')}>
              <FlipButton onClick={this.handleClickFlip}>К характеристикам</FlipButton>
            </div>
          </div>
        ) : null}

        {swiper ? (
          <div className={cx('card', 'back')}>
            {active && (
              <button className={cx('close')} onClick={onClose}>
                <i className={cx('fa', 'fa-2x', 'fa-close', 'icon')} />
              </button>
            )}
            <div className={cx('backHead')}>
              {images &&
                !!images.length && (
                  <div
                    className={cx('backImage')}
                    style={{ backgroundImage: `url(${images[0]})` }}
                  />
                )}
              <div className={cx('backTitle')}>{name}</div>
            </div>
            <div className={cx('backParams')}>
              {intags ? (
                <Parameters
                  {...{
                    id,
                    intags,
                    remains,
                    article,
                  }}
                />
              ) : (
                <div>loading...</div>
              )}
            </div>
            <div className={cx('buttonWrapper', 'flipButton')}>
              <FlipButton onClick={this.handleClickFlip}>К фотографиям</FlipButton>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

Card.defaultProps = {
  active: false,
  data: {},
  intags: {},
  onClose: () => console.log('[onClose]'), // eslint-disable-line
  remains: {},
  swiper: false,
};

Card.propTypes = {
  data: PropTypes.object,
  intags: PropTypes.object,
  remains: PropTypes.object,
  active: PropTypes.bool,
  swiper: PropTypes.bool,
  onClose: PropTypes.func,
};

const mapStateToProps = ({ products }) => ({
  intags: products.intags,
  remains: products.remains,
});

export default connect(mapStateToProps)(Card);
