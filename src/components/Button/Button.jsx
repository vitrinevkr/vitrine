import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './Button.pcss';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

const Button = ({ href, onClick, children, className, icon, wide, hidden, ...props }) => {
  const Tag = href ? Link : 'button';

  return (
    <div className={cx('wrapper', { wide, hidden })}>
      <Tag className={cx('button', className)} to={href} onClick={onClick} {...props}>
        <span className={cx('text')}>
          {icon && <i className={cx('fa', `fa-${icon}`, 'icon')} />}
          {children}
        </span>
      </Tag>
    </div>
  );
};

Button.defaultProps = {
  hidden: false,
  target: '_self',
  icon: '',
  href: '',
  wide: false,
  onClick: () => false,
};

Button.propTypes = {
  href: PropTypes.string,
  target: PropTypes.oneOf(['_self']),
  onClick: PropTypes.func,
  icon: PropTypes.string,
  wide: PropTypes.bool,
  hidden: PropTypes.bool,
};

export default Button;
