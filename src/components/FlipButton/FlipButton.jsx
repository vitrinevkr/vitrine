import React from 'react';
import PropTypes from 'prop-types';

import Button from 'src/components/Button/Button';

const FlipButton = ({ onClick, children, className, hidden, disabled }) => {
  return (
    <Button className={['small', className]} icon="cube" onClick={onClick}>
      {children}
    </Button>
  );
};

FlipButton.defaultProps = {
  hidden: false,
  disabled: false,
};

FlipButton.propTypes = {
  disabled: PropTypes.bool,
  hidden: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

export default FlipButton;
