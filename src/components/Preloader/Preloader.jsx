import React from "react";
import PropTypes from "prop-types";

import Icon from "src/components/Icon/Icon";

import classNames from "classnames/bind";
import styles from "./Preloader.pcss";
const cx = classNames.bind(styles);

const Preloader = ({ scale, className }) => (
  <span className={cx(className, "preloader")}>
    <Icon name="preloader" scale={scale} className={cx("image")} />
  </span>
);

Preloader.defaultProps = {
  className: "",
  scale: 1
};

Preloader.propTypes = {
  className: PropTypes.string,
  scale: PropTypes.number
};

export default Preloader;
