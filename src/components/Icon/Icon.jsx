import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames/bind';
import styles from './Icon.pcss';
const cx = classNames.bind(styles);

const sizes = {
  preloader: { width: 39, height: 39 },
};

const Icon = ({ name, className, scale }) => {
  const size = sizes[name];
  if (!size) {
    return null;
  }

  return (
    <svg
      className={cx('layout', className)}
      width={size.width * scale}
      height={size.height * scale}
    >
      <use xlinkHref={`#icon-${name}`} />
    </svg>
  );
};

Icon.defaultProps = {
  scale: 1,
  className: '',
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  scale: PropTypes.number,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.object]),
};

export default Icon;
