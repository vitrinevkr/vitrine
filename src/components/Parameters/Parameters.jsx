import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames/bind';
import styles from './Parameters.pcss';
const cx = classNames.bind(styles);

const Color = value => {
  const color = /(.+);(#[a-f0-9]{6})/i.exec(value);
  if (!color || !color[1]) {
    return value;
  }

  return (
    <div>
      {color[1]} <span className={cx('color')} style={{ background: color[2] }} />
    </div>
  );
};

const Parameters = ({ id, intags }) => {
  const main = [];
  const all = [];

  if (!id || !intags || !intags.length) {
    return null;
  }

  intags.forEach((item, key) => {
    const elements = [];

    item.intags.forEach(intag => {
      let value = '';

      if (intag.id === 21) {
        value = Color(intag.value[0]);
      } else {
        value = intag.value.join(', ');
      }

      main.push(
        <tr key={`spc-${intag.id}`}>
          <td>{intag.name}</td>
          <td>{value}</td>
        </tr>
      );
    });

    if (elements.length) {
      all.push(
        <tr key={`th-${key}`}>
          <th colSpan="2">{item.name}</th>
        </tr>,
        elements.slice(0, 10)
      );
    }
  });

  return (
    <div className={cx('params')}>
      <table>
        {main.length ? (
          <tbody>
            <tr>
              <th colSpan="2" className={cx('title', 'mainTitle')}>
                Основные характеристики
              </th>
            </tr>
          </tbody>
        ) : null}

        {main.length ? <tbody>{main}</tbody> : null}

        {all.length ? (
          <tbody>
            <tr>
              <th colSpan="2" className={cx('title')}>
                Все характеристики
              </th>
            </tr>
          </tbody>
        ) : null}

        {all.length ? <tbody>{all}</tbody> : null}
      </table>
    </div>
  );
};

Parameters.defaultProps = {
  id: null,
  intags: [],
  remains: 0,
};

Parameters.propTypes = {
  id: PropTypes.number,
  intags: PropTypes.array,
  remains: PropTypes.number,
};

export default Parameters;
