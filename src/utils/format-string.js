export function price(number) {
  if (typeof number !== 'number') {
    return number;
  }

  if (number > 9999) {
    return `${number} ₽`.replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
  }

  return `${number} ₽`;
}

export function productName(name) {
  if (typeof name !== 'string') {
    return name;
  }

  return name.replace(
    /^(Смартфон|Мобильный телефон|Планшет|Роутер|Умные часы|Карта памяти|Наушники|Чехол-аккумулятор|Чехол-книжка|Клип-кейс|Чехол спортивный|Чехол-клавиатура|Флип-кейс|Чехол-футляр|Защитная|Защитное|защитных|Портативное зарядное устройство|Чехол-аккумулятор|Зарядное устройство|USB-накопитель|Портативная колонка|Колонка)/i,
    ''
  );
}
