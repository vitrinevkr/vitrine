import { SPEECH, SPEECH_IDS } from 'src/constants/settings';
import { CATALOG_NAMES } from 'src/stubData/vitrine';

const commands = {
  экран: 1,
};

const ext = {
  0: 'stop',
  1: 'home',
};

function clearSearchText(value) {
  const v = value.toLowerCase().trim();
  const c = v.split(' ');
  if (commands[c[0]]) {
    return c.slice(1).join(' ');
  }
  return v;
}

export function checkSpeech(value) {
  const v = clearSearchText(value);
  const id = SPEECH[v];
  if (!id && !CATALOG_NAMES[v]) {
    return '';
  }

  return (
    ext[id] || CATALOG_NAMES[v] || CATALOG_NAMES[SPEECH_IDS[id].toLowerCase()]
  );
}
