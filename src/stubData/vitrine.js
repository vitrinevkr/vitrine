export const CATALOG_IDS = {
  55: 'Все телефоны',
  60: 'Apple iPhone',
  51: 'Android-смартфоны',
  164: 'Другие смартфоны',
  163: 'Кнопочные телефоны',
  32: 'Планшеты',
  270: 'Умные часы',
  274: 'Модемы',
  275: 'Все аксессуары',
  252: 'Наушники',
  276: 'Чехлы и сумки',
  278: 'Карты памяти',
  280: 'Зарядные устройства',
  281: 'USB-накопители',
  282: 'Колонки',
};

export const MENU = [
  {
    id: 55,
    icon: 'mobile',
  },
  {
    id: 32,
    icon: 'tablet',
  },
  {
    id: 275,
    icon: 'headphones',
  },
];

export const VITRINE = [
  {
    id: 187500,
    images: [
      '/assets/catalog/55a354ee-41a0-4a91-81fb-b9607ed23ca0.png',
    ],
    catalogId: 270,
    parentCatalogId: 275,
    name: 'Apple Watch',
  },
  {
    id: 199602,
    images: [
      '/assets/catalog/592e0b65-1c6c-4a6c-af25-e02ffca0d7dd.png',
      '/assets/catalog/06559933-9b67-4a9f-a255-3a39fb1f9e3e.png',
      '/assets/catalog/2654d60c-27eb-4d92-8503-050d69df4e80.png',
      '/assets/catalog/c21170a6-b347-45b1-a774-596206004bcf.png',
    ],
    catalogId: 282,
    parentCatalogId: 275,
    name: 'Ginzzu GM-999C Black',
  },
  {
    id: 189069,
    images: [
      '/assets/catalog/beefe172-de4f-4e10-b940-e2c9e2a09b21.png',
    ],
    catalogId: 252,
    parentCatalogId: 275,
    name: 'Marshall Major II Black',
  },
  {
    id: 196174,
    images: [
      '/assets/catalog/60d08bb4-086f-480b-9a7b-59146d987ade.png',
    ],
    catalogId: 276,
    parentCatalogId: 275,
    name: 'Клип-кейс Apple Leather Case для iPhone 7 Plus Saddle Brown',
  },
  {
    id: 203092,
    images: [
      '/assets/catalog/ca0ab365-03b7-4643-810d-c9f11bbbc975.png',
    ],
    catalogId: 51,
    parentCatalogId: 55,
    name: 'Samsung Galaxy J2 Prime Black',
  },
  {
    id: 196982,
    images: [
      '/assets/catalog/56eab360-5285-4678-82af-43caf9581bd4.png',
    ],
    catalogId: 60,
    parentCatalogId: 55,
    name: 'Apple iPhone 8 Plus 64GB Серебристый',
  },
  {
    id: 195199,
    images: [
      '/assets/catalog/733f91b5-3e89-4e9c-975e-7c93ca820c68.png',
    ],
    name: 'Meizu Pro6 64Gb Dark Gray',
    catalogId: 164,
    parentCatalogId: 55,
  },
  {
    id: 201022,
    images: [
      '/assets/catalog/8ab45343-fbbb-4546-ad0c-bb71822d134e.png',
    ],
    catalogId: 163,
    parentCatalogId: 55,
    name: 'Мобильный телефон Nokia 3310 (2017) Dual SIM Dark Blue',
  },
  {
    id: 194017,
    images: [
      '/assets/catalog/dcaa77e9-52a5-4b19-b8e0-7f89f6e9b72f.png',
    ],
    catalogId: 32,
    parentCatalogId: 32,
    name: 'Планшет Samsung Galaxy Tab A 7.0 SM-T285N 8Gb LTE Silver',
  },
  {
    id: 117342,
    images: [
      '/assets/catalog/bf7447cc-b322-4062-ba02-a2170e20949c.png',
    ],
    name: 'USB-накопитель Leef iBridge 64Gb Black',
    catalogId: 281,
    parentCatalogId: 275,
  },
  {
    id: 110860,
    images: [
      '/assets/catalog/e3710770-442b-4dc0-abaa-6339ecb402d4.png',
    ],
    name: 'InterStep PB16800 Black/Red',
    catalogId: 280,
    parentCatalogId: 275,
  },
];

export const CATALOG = VITRINE.reduce(
  (c, v) => ({
    ...c,
    [v.parentCatalogId]: [
      ...(c[v.parentCatalogId] || []),
      { id: v.catalogId, title: CATALOG_IDS[v.catalogId] },
    ],
  }),
  {}
);

export const CATALOG_NAMES = Object.entries(CATALOG_IDS).reduce(
  (c, [id, v]) => ({ ...c, [v.toLowerCase()]: id }),
  {}
);
