import './styles/base.pcss';
import './styles/swiper.css';

import React from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { history } from 'src/utils/history';

import Catalog from 'src/containers/Catalog/Catalog';
import Menu from 'src/containers/Menu/Menu';
import Speech from 'src/containers/Speech/Speech';
import Vitrine from 'src/containers/Vitrine/Vitrine';
import Test from 'src/containers/Test/Test';

export default (
  <ConnectedRouter history={history}>
    <Route
      render={({ location }) => (
        <CSSTransitionGroup
          className="reactRoot"
          component="div"
          transitionName="router"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={500}
        >
          <Switch key={location.key} location={location}>
            <Route path="/catalog/:catalogId/:id?" component={Catalog} history={history} />
            <Route path="/img" component={Test} />
            <Route path="*" component={Vitrine} />
          </Switch>
          <Menu history={history} location={location} />
          <Speech history={history} location={location} />
        </CSSTransitionGroup>
      )}
    />
  </ConnectedRouter>
);
