import { combineReducers } from 'redux';
import products from 'src/containers/Catalog/reducer';
import vitrine from 'src/containers/Vitrine/reducer';
import menu from 'src/containers/Menu/reducer';
import speech from 'src/containers/Speech/reducer';
import { routerReducer } from 'react-router-redux';

const reducers = combineReducers({
  menu,
  products,
  routing: routerReducer,
  speech,
  vitrine,
});

export default reducers;
