const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const Visualizer = require('webpack-visualizer-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    src: [path.resolve(__dirname, '../src/index')],
  },
  output: {
    path: path.resolve(__dirname, '../build'),
    publicPath: '/',
    filename: '[name].js',
  },
  resolve: {
    alias: {
      src: path.resolve(__dirname, '../src'),
      react: path.resolve('./node_modules/react'),
      'react-dom': path.resolve('./node_modules/react-dom'),
      utils: path.resolve(__dirname, '../src/utils'),
    },
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          // 'eslint-loader',
        ],
      },
      {
        test: /\.p?css$/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                importLoaders: 1,
                localIdentName: '[name]_[local]_[hash:base64:4]',
              },
            },
            {
              loader: 'postcss-loader',
            },
          ],
        }),
      },
      {
        test: /\.svg$/,
        loader: 'babel-loader!svg-react-loader',
      },
    ],
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      test: /\.p?css/,
      minimize: true,
      debug: false,
      options: {
        postcss: [
          require('postcss-import')({ addDependencyTo: webpack }),
          require('postcss-url')({ url: 'inline' }),
          require('postcss-cssnext')({ browsers: ['last 2 versions'] }),
        ],
      },
    }),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(true),
      PACKAGE_VERSION: JSON.stringify(require('../package.json').version),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
      },
      output: {
        comments: false,
      },
      sourceMap: 'nosources-source-map',
    }),
    new ExtractTextPlugin('[name].css'),
    new HtmlPlugin({
      template: path.resolve(__dirname, '../index.html'),
    }),
    new CopyWebpackPlugin([
      { from: path.resolve(__dirname, '../assets'), to: 'assets' },
    ]),
    new Visualizer(),
  ],
  devtool: 'source-map',
};
