const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    src: [
      'babel-polyfill',
      'webpack-hot-middleware/client?reload=true',
      path.resolve(__dirname, '../src/index'),
    ],
  },
  output: {
    path: path.resolve(__dirname, '../build'),
    publicPath: '/',
    filename: '[name].js',
  },
  resolve: {
    alias: {
      src: path.resolve(__dirname, '../src'),
    },
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          // 'eslint-loader',
        ],
      },
      {
        test: /\.p?css$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64:2]',
            },
          },
          'postcss-loader',
        ],
      },
      {
        test: /\.svg$/,
        loader: 'babel-loader!svg-react-loader',
      },
    ],
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      test: /\.p?css/,
      minimize: false,
      debug: false,
      options: {
        postcss: [
          require('postcss-import')({ addDependencyTo: webpack }),
          require('postcss-url')({ url: 'inline' }),
          require('postcss-cssnext')({ browsers: ['last 2 versions'] }),
        ],
      },
    }),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new webpack.DefinePlugin({
      PACKAGE_VERSION: JSON.stringify(require('../package.json').version),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlPlugin({
      template: path.resolve(__dirname, '../index.html'),
    }),
  ],
  devtool: 'eval',
};
